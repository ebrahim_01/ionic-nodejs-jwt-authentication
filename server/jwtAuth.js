var express = require('express'),
    jwt = require('express-jwt'),
    _ = require('lodash'),
    config = require('./config'),
    jwtwt = require('jsonwebtoken');;


/** For create the token */

//create token "signeture"
exports.createIdToken = function(user) {
    return jwtwt.sign(_.omit(user, 'password'), config.secret, { expiresIn: 60 * 60 * 5 });
}






/**
*  What are "Claims"?

* Claims are the predefined keys and their values:

* iss: issuer of the token
* exp: the expiration timestamp (reject tokens which have expired). Note: as defined in the spec, this must be in seconds.
* iat: The time the JWT was issued. Can be used to determine the age of the JWT
* nbf: "not before" is a future time when the token will become active.
* jti: unique identifier for the JWT. Used to prevent the JWT from being re-used or replayed.
* sub: subject of the token (rarely used)
* aud: audience of the token (also rarely used)
 */
exports.createAccessToken = function() {
        return jwtwt.sign({
            iss: config.issuer,
            aud: config.audience,
            exp: Math.floor(Date.now() / 1000) + (60 * 60),
            scope: 'full_access',
            sub: "lalaland|gonto",
            jti: genJti(), // unique identifier for the token
            alg: 'HS256'
        }, config.secret);
    }
    // Generate Unique Identifier for the access token
function genJti() {
    let jti = '';
    let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (let i = 0; i < 16; i++) {
        jti += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return jti;
}





/** checked for protected API */


// Validate access_token
exports.jwtCheck = jwt({
    secret: config.secret,
    aud: config.audience,
    iss: config.issuer,
    getToken: function fromHeaderOrQuerystring(req) {
        return req.headers.authorization.split(' ')[1];
    }
});
// Check for access privilige
exports.roleAuthorization = function(roles) {

    return function(req, res, next) {
        console.log(req.user);
        /*     //real app
              //call the db to get this user by ID 
                 User.findById(req.user._id,function(err,foundUser){
                  if(err){
                  res.status(422).json({error: 'No user found.'});
                  return next(err);
              }

              if(roles.indexOf(foundUser.role) > -1){
                  return next();
              }

              res.status(401).json({error: 'You are not authorized to view this content'});
              return next('Unauthorized');
               });*/


        if (roles.indexOf('full_access') > -1) {
            return next();
        }
        res.status(403).send('You are not authorized to view this content');
        return next('Unauthorized');

    };
}