var express = require('express'),
    config = require('./config'),
    jwtAuth = require('./jwtAuth'),
    quoter = require('./quoter');

var app = module.exports = express.Router();








app.use('/api/protected', jwtAuth.jwtCheck, jwtAuth.roleAuthorization(['full_access']));

app.get('/api/protected/random-quote', function(req, res) {
    res.status(200).send(quoter.getRandomOne());
});