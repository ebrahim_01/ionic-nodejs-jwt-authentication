import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';


import { JwtHelper } from 'angular2-jwt';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Network } from '@ionic-native/network';
import { LoadingController } from 'ionic-angular';
import { User } from "../Model/User";


@Injectable()
export class AuthService {
  
 public user: User = new User();
 
  private LOGIN_URL = "http://192.168.178.37:3001/sessions/create";
  private SIGNUP_URL = "http://192.168.178.37:3001/users";
  //public static Athenticated;
  contentHeader = new Headers({ "Content-Type": "application/json" });
  jwtHelper = new JwtHelper();
  loader;
  constructor(public loadingCtrl: LoadingController, private net: Network, private http: Http, public storage: Storage) {
    console.log('Hello AuthService Provider');
    //need 2 seconds to finish
   this.getAuthUser().then(user => this.user = user);


  }

getUsr():User{
  return this.user;
}
  presentLoading(msg: string) {
    this.loader = this.loadingCtrl.create({
      content: msg
    });
    this.loader.present();
  }
  dismissLoadinf() {
    this.loader.dismiss();
  }



  noConnection() {
    //this.platform.ready().then(()=>{return (<string>Network.connection==='none')});
    console.log(this.net.type);

    return (this.net.type == 'none');
  }



























  /**
   *  this work but you cannot assign var from out the promise methode 
   *  static because is checked in ionViewCanEnter before an instance from this server is contructed
   */

  public static isAthenticated(): boolean {
    
// if(this.user.exp===0){
//  if (localStorage.getItem('exp') === null) {
//      return false;
//       //   console.log('nononon'); return this.Athenticated; 
//     }
//     else {
//       let exp = localStorage.getItem('exp');
//       // console.log('exp s:' + new Date(1495067623).toString());
//       //console.log('date now:' + Date.now());
//       if (Date.now() < +exp) {
//       return true;
      
//       } else {
//       return false;
       
//       }
//     };
// }
// else{
// if (Date.now() < +this.user.exp) {
//       return true;
//          return true;
//        } else {
//        return false;
       
//        }

// }



    if (localStorage.getItem('exp') === null) {
      return false;
      //   console.log('nononon'); return this.Athenticated; 
    }
    else {
      let exp = localStorage.getItem('exp');
      // console.log('exp s:' + new Date(1495067623).toString());
      //console.log('date now:' + Date.now());
      if (Date.now() < +exp) {
     return true;
       
      } else {
     return false;
        
      }
     };
  }



  getExp(): any {

    return this.storage.get('id_token').then((val) => {
      return this.jwtHelper.decodeToken(val).exp * 1000 + new Date().getTime();


    }).catch(err => console.log('no token'));

  }














  login(credentials): Observable<any> {

    return this.http.post(this.LOGIN_URL, credentials, { headers: this.contentHeader })
      .map(res => res.json())
      .catch(this.handleError);


  }



  signup(credentials): Observable<any> {
    return this.http.post(this.SIGNUP_URL, credentials, { headers: this.contentHeader })
      .map(res => res.json())
      .catch(this.handleError);
  }




  logout() {
    console.log('log out call');

    this.storage.remove('id_token');
    localStorage.removeItem('exp');

    // this.storage.ready().then(() => {
    //   this.storage.get('id_token').then(token => {
    //     console.log('id_token' + token);
    //   }).catch(console.log);
    // });


    this.user = new User();
//User=new User();
  }














  authSuccess(token) {

    this.storage.set('id_token', token);
    //console.log('from serv. ' + this.jwtHelper.decodeToken(token).exp);
    localStorage.setItem('exp', JSON.stringify((this.jwtHelper.decodeToken(token).exp * 1000) + new Date().getTime()));
    console.log('token ' + this.jwtHelper.decodeToken(token).toString());

    this.user = new User(this.jwtHelper.decodeToken(token).username.toString(), this.jwtHelper.decodeToken(token).id,this.jwtHelper.decodeToken(token).exp,token) || new User();
  //User = new User(this.jwtHelper.decodeToken(token).username.toString(), this.jwtHelper.decodeToken(token).id,this.jwtHelper.decodeToken(token).exp) || new User();
    console.log('authsuccess ' + JSON.stringify(this.user));

    this.storage.set('profile', this.user);


  }


  // public getAuthUser(fn, err) {

  //   this.storage.get('profile').then((user) => {
  //     fn(user);
  //   }).catch((err) => console.log(err));

  // }
  getAuthUser(): Promise<any> {

    return new Promise((resolve, reject) => {
      if (this.user.id !== -1){
       console.log('from user obj');
        resolve(this.user)}
      else
        this.storage.get('profile').then((user) => {
          resolve(user);
        }).catch((err) => reject(err));


    });

  }


  // static getCurrentUser(){
  //   return AuthService.user;
  // }

























  private handleError(error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    //Response{_body: "The username or password don't match", status: 401, ok: false, statusText: "Unauthorized", headers: Headers…}
    let errMsg: string;
    // console.log("err="+error);
    //  const err = body.error || JSON.stringify(body);
    if(error.status===0)
    errMsg="The request UNSENT";
    else if(error.status===1)
   errMsg="The request OPENED but not sent" ;
   else
    errMsg = `${error._body}` || "Unknown error !!" ;
    // errMsg = error.message ? error.message : error.toString();
    
    console.error("errMsg= " + JSON.stringify(errMsg));
    return Observable.throw(errMsg);

  }
}
