import { Injectable } from '@angular/core';
import { Http, Headers, Response  } from '@angular/http';


import { AlertController } from 'ionic-angular';
import { JwtHelper } from 'angular2-jwt';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {AuthService} from './auth-service';
import { User } from "../Model/User";
/*
  Generated class for the DataService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class DataService {
BASE_API_URL='http://localhost:3001/api';
normalheader = new Headers({ "Content-Type": "application/json" });
authHeadr=new Headers();

  constructor(public authService:AuthService,private alertCtrl: AlertController, public http: Http, public storage: Storage) {
    console.log('Hello DataService Provider');

  }

presentAlert(title,subTitiel='') {
  let alert = this.alertCtrl.create({
    title: title,
    subTitle: subTitiel,
    buttons: ['ok']
  });
  alert.present();
}


//get protected data
 getSecretQuote():Observable<any> {
    // Use authHttp to access secured routes
    return new Observable(observer=>{
    this.storage.get('id_token').then((token) => {
    console.log('token from storage: '+token);
     
      let headers = new Headers();
      headers.append('Authorization', 'Bearer ' + token);

      this.http.get(`${this.BASE_API_URL}/protected/random-quote`, {
        headers: headers
      }).map(res => res.text())
        .subscribe(
          data =>{observer.next(data); observer.complete();},
          err => {console.log(err); observer.error(this.handleError(err))}
        );
    }).catch(err=>observer.error(err))
    });
  }


//get protected data
 getNotSecQuote(): Observable<any> {


   return this.http.get(`${this.BASE_API_URL}/random-quote`)
       .map(res =>res.text())
      .catch(this.handleError);

    
  }









 public handleError(error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    //Response{_body: "The username or password don't match", status: 401, ok: false, statusText: "Unauthorized", headers: Headers…}
    let errMsg: string;
    // console.log("err="+error);
    //  const err = body.error || JSON.stringify(body);
   console.log(error);
   
   if(error===false)
   errMsg="Storage error";
   else{
    if(error.status===0)
    errMsg="The request UNSENT";
    else if(error.status===1)
   errMsg="The request OPENED but not sent" ;
   else if(error.status===401)
    errMsg = "You don't have Authorization" || "Unknown error !!" ;
    
    else
    errMsg= `${error._body}`;
   }
    // errMsg = error.message ? error.message : error.toString();
    
    console.error("errMsg= " +errMsg);
    return Observable.throw(errMsg);

  }



}
