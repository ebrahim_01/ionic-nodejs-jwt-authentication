import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { Profile } from '../pages/profile/profile';
import { AuthService } from '../providers/auth-service';
import { DataService } from '../providers/data-service';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { Network } from '@ionic-native/network';
/*
// Configuration for JWT Auth. based
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
let storage:Storage;
export function getAuthHttp(http) {
  return new AuthHttp(new AuthConfig({
    //headerPrefix: YOUR_HEADER_PREFIX,
    noJwtError: true,
    globalHeaders: [{'Accept': 'application/json'}],
    tokenGetter: (() => storage.get('token')),
  }), http);
}
//end

*/
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    Profile
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    Profile
  ],
  providers: [
   // AuthConfig,
  //    useValue: new AuthConfig({
  //             headerName: 'Authorization',
  //             headerPrefix: 'Bearer ',
  //             tokenName: 'auth_token',
  //             tokenGetter: (() => localStorage.getItem('auth_token')),
  //             globalHeaders: [{ 'Content-Type': 'application/json' }],
  //             noJwtError: true,
  //             noTokenScheme: true
  // })},
  //  AuthHttp,
    StatusBar,
    SplashScreen,
    AuthService,
    DataService,
    Network,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
