import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// import { AuthConfig, AuthHttp } from "angular2-jwt/angular2-jwt";
// import {Http} from '@angular/http';
// import {provide} from '@angular/core';

import { HomePage } from '../pages/home/home';
import { Profile } from '../pages/profile/profile';
import { AuthService } from "../providers/auth-service";
@Component({
  templateUrl: 'app.html',
  // providers: [
  //   provide(AuthHttp, {
  //     useFactory: (http) => {
  //       return new AuthHttp(new AuthConfig, http);
  //     },
  //     deps: [Http]
  //   })
  // ]
})
export class MyApp {

 rootPage:any ;

  constructor( platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
if(AuthService.isAthenticated())
this.rootPage=Profile;
else
this.rootPage=HomePage;
    });
  }
}

