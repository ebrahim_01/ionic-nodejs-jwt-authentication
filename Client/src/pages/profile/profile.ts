import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AuthService } from "../../providers/auth-service";
import { DataService } from "../../providers/data-service";
import { HomePage } from '../home/home';
import { User } from "../../Model/User";
/**
 * Generated class for the Profile page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class Profile {
  user: User = new User();
unProtectedData:string='';
protectedData:string='';
  //user=AuthService.user;
  constructor(public dataService: DataService, public authService: AuthService, public navCtrl: NavController, public navParams: NavParams) {

    console.log('ionViewDidLoad Profile 1');
  }


  logout() {
    this.authService.presentLoading('Loading...');
    this.authService.logout();
    this.navCtrl.setRoot(HomePage);
    this.authService.dismissLoadinf();

  }
  goback() {
    this.navCtrl.push(HomePage).catch(err => console.log(err.message));
  }
  ionViewCanEnter() {
    return new Promise((resolve, reject) => {
      if (AuthService.isAthenticated()) {

        resolve('true');
      }
      else {
        this.dataService.presentAlert('Access', 'Cannot access you must login first');
        reject(new Error('not accessable'));
      }
    });

  }
  ionViewDidEnter() {
    console.log('ionViewDidLoad Profile 3');
    this.authService.getAuthUser().then(user => { this.user = user; }).catch(err => { this.user = <User>{} });
    //this.username=this.authService.gett();
  }




  getAuthData(){

this.dataService.getSecretQuote().subscribe( data=>{this.protectedData=data;},err=>this.dataService.presentAlert('Error',err.error));
  }
  getNormalData(){
this.dataService.getNotSecQuote().subscribe( data=>{this.unProtectedData=data; },err=>this.dataService.presentAlert('Error',err.error));
  }
}
