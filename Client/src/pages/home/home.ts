import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { AuthService } from "../../providers/auth-service";
import { DataService } from "../../providers/data-service";
import { Storage } from '@ionic/storage';
import { User } from "../../Model/User";
import { NgZone } from '@angular/core';
import { Profile } from '../profile/profile';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  authenticated: boolean = AuthService.isAthenticated();
  authType: string = "login";
  user: User=new User();
  //user=User;
  zone;
  u={name:'eee'};
  constructor(public dataService: DataService, public storage: Storage, public authService: AuthService, public navCtrl: NavController) {
    this.zone = new NgZone({ enableLongStackTrace: false });
    console.log('11');

  }
  authenticate(credentials) {
    this.authType == 'login' ? this.login(credentials) : this.signup(credentials);
  }
  login(credentials) {
    this.authService.presentLoading('Loading...');
    if (this.authService.noConnection()) {
      this.dataService.presentAlert('Connection error', 'No connection found !!');
      this.authService.dismissLoadinf();
    }
    else this.authService.login(JSON.stringify(credentials)).subscribe(data => { this.authService.authSuccess(data.id_token); this.authService.dismissLoadinf(); this.navCtrl.setRoot(Profile).catch((err) => console.log('errrrrrr cannot load the Profile view')); }
      , err => { this.dataService.presentAlert('Error', JSON.stringify(err)); this.authService.dismissLoadinf(); });

  }
  signup(credentials) {
    this.authService.presentLoading('Loading...');
    if (this.authService.noConnection()) {
      this.dataService.presentAlert('Connection error', 'No connection found !!');
      this.authService.dismissLoadinf();
    }
    else
      this.authService.signup(JSON.stringify(credentials)).subscribe(data => { this.authService.authSuccess(data.id_token); this.authService.dismissLoadinf(); this.dataService.presentAlert('Success', 'You have successfull signed up and in <br /> click ok to continue->'); this.navCtrl.setRoot(Profile).catch(() => console.log('errrrrrr')); }, err => { this.dataService.presentAlert('Error', JSON.stringify(err)); this.authService.dismissLoadinf(); });
  }
  ionViewDidLoad() {
    console.log('33');

  }
  ionViewWillEnter() {
    //call every time even if cached
    console.log('444');
   // this.authenticated = AuthService.isAthenticated();
   this.authService.getAuthUser().then(user=>{this.user=user; }).catch(err=>{console.log(err);});
  }

  ionViewCanEnter() {
    console.log('222');
    return new Promise((resolve, reject) => {
      if (!AuthService.isAthenticated()) {
        resolve(true);
      }
      else {
        this.dataService.presentAlert('access', 'cannot return !');
        reject(new Error('Not authorised'));
      }
    });


  }


  logout() {
    this.authService.presentLoading('Loading...');
    this.zone.run(() => {
      this.authService.logout();
      this.authenticated = AuthService.isAthenticated();
      this.authService.dismissLoadinf();
    });
    //this.authService.getExp().then(exp=>{this.authenticated=Date.now() < exp;});

    //this.navCtrl.push(HomePage);
  }

}
